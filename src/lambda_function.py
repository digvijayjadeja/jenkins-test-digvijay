import json
import os
from datetime import datetime, timezone

import boto3

from solutions.load_config import load_config, load_exclusions
from solutions.parsing import (parse_session_id, parse_price_string, parse_cart_economics)
from solutions.utils import (get_audit_details, get_session_state, reload_caching)
from solutions.veo3 import VEO3

lambda_client = boto3.client('lambda')
dynamodb = boto3.resource('dynamodb')
# feature_store_client = boto3.client('sagemaker-featurestore-runtime') # wk
# SESSION_FEATURE_GROUP_NAME = os.environ['SESSION_FEATURE_GROUP_NAME'] # wk
COUPON_TABLE_NAME = os.environ['COUPON_TABLE_NAME']
COUPON_TABLE_INDEX_NAME = os.environ['COUPON_TABLE_INDEX_NAME']

#adding last time we fetched from dynamodb

configuration = load_config(dynamodb)
caching_expiration = configuration['general']['caching_expiration']
excluded_category_ids = load_exclusions(dynamodb)
last_update_time = datetime.now()


def create_offer(event):
    global last_update_time
    global configuration
    global excluded_category_ids
    print("before: last_update_time: ", last_update_time)
    audit_details = list()
    
    if reload_caching(last_update_time, caching_expiration):
        configuration = load_config(dynamodb)
        excluded_category_ids = load_exclusions(dynamodb)
        last_update_time = datetime.now()
        print("cache released: ", last_update_time)
    
    offer_context = event['userRequest']['offerContext']
    user_type = event['userRequest']['account']['UserType']
    user_type = user_type.upper()
    audit_details += [get_audit_details("USER_TYPE:",
                                        f"INFO: The user type is : {user_type}.")]

    #disable_offer = configuration["general"]["disable_offer"]
    disable_offer = False
    disable_offer_list = configuration["general"]["disable_offer_list"]
    try:
        for startdate,enddate,reason in disable_offer_list:
            srt_time_obj = datetime.strptime(startdate, '%d %b %Y %H:%M:%S %z')
            end_time_obj = datetime.strptime(enddate, '%d %b %Y %H:%M:%S %z')
            dnow = datetime.now(timezone.utc)
            if(srt_time_obj < dnow < end_time_obj):
                disable_offer = True
                break
    except:
        raise ValueError("Value error exception in disable_offer_list")
    session_id = parse_session_id(event)

    account_id = event['userRequest']['account']['AccountID']
    user_id = event["userRequest"]["offerContext"]["eventPayload"]["userID"]
    audit_details += [get_audit_details("USER_INFO:",
                                        f"INFO: AccountID: {account_id}.\
                                        UserID: {user_id}.\
                                        SessionID {session_id}.")]

    session_state = get_session_state(session_id)

    audit_details += [get_audit_details("SESSION_STATE_IN_FEATURE_STORE",
                                        f"INFO: The session state from the feature store: {json.dumps(session_state)}.")]

    try:
        cart_data = offer_context['eventPayload']['detail']['cartData']
    except KeyError:
        cart_data = dict()

    try:
        cart_total = parse_price_string(offer_context['eventPayload']['detail']['cartTotal'])
    except KeyError:
        cart_total = 0.0

    # Get the current cart economics (cart total, profit margin, eligible total, etc...) 
    # before deciding on the offer type.
    cart_economics = parse_cart_economics(cart_data, configuration, dynamodb, excluded_category_ids, audit_details)

    print('Cart Economics', cart_economics)
    if round(cart_economics['cart_total']) != round(cart_total):
        audit_details += [get_audit_details("BAD_CART_DATA",
                                            f"WARNING: cartTotal ({cart_total}) does not match the value from cartData ({cart_economics['cart_total']}).")]

    coupon_types = dict()
    coupon_codes = dict()

    client_name = configuration["general"]['client']
    solution = VEO3(lambda_client, dynamodb, configuration)
    # Get the model predictions.
    # model_outputs = solution.get_model_outputs(session_id)
    # WK: decoupling dependency on model output from test-runtime-feature-store-query
    model_outputs = solution.failsafe_response
    model_prediction_datetime_utc = datetime.utcnow().isoformat()
    # print(f'Model Outputs ({client_name})', model_outputs)

    # Decide whether to present a stretch offer, a conversion offer, or a pre-ATC offer.
    offer_type = solution.decide_on_offer_type(model_outputs, session_state, offer_context)
    # print(f'Offer Type ({solution.__class__.__name__})', offer_type)

    if offer_type is not None:
        spend, reward_value, variant = solution.get_spend_and_reward(offer_type, cart_economics, model_outputs, session_state, offer_context, excluded_category_ids)
        # print(f"Spend ({solution.__class__.__name__})", spend[solution_name])
        # print(f"Reward ({solution.__class__.__name__})", reward_value[solution_name])

        if spend is not None and reward_value is not None:
            coupon_types = solution.select_coupon_types(spend, reward_value)
            coupon_codes = [coupon_type['CouponCode'] for coupon_type in coupon_types]
        else:
            coupon_types = list()

    audit_details += solution.audit_details

    if offer_type is not None and coupon_codes is not None and coupon_codes \
            and user_type in configuration['user_types']['offer_presentment_user_types'] \
            and not disable_offer:
        # TODO: Remove the outer OfferType after Priyanka removes it as a requirement.
        response = {
            'OfferType': offer_type,
            'RewardValue': float(coupon_types[0]['RewardValue']),
            'Spend': float(coupon_types[0]['MinimumSpend']),
            'Away': float(coupon_types[0]['MinimumSpend']),
            'Progress': 0,
            'CouponTypes': coupon_codes,
            'Offer': {
                'OfferType': offer_type,
                'Variant': variant,
                'AuditDetails': audit_details
            },
            'ModelData': {
                'ModelMetaData': {
                    'Solution': client_name,
                    'ModelEndpointName': solution.model_endpoint_name,
                    # 'SessionFeatureGroupName': SESSION_FEATURE_GROUP_NAME,
                    'PredictionDatetimeUTC': model_prediction_datetime_utc,
                    'OfferType': offer_type
                },
                'ModelOutputs': model_outputs,
                'ModelInput': session_state
            }
        }
    else:
        if offer_type is None:
            audit_details += [get_audit_details("NO_OFFER",
                                                f"INFO: Offer type is none.")]
        elif disable_offer:
            #reason = configuration["general"]["disable_offer_reason"]
            audit_details += [get_audit_details("NO_OFFER",
                                                f"INFO: Offer is disabled. Reason: {reason}")]
        elif coupon_codes is None or not coupon_codes:
            audit_details += [get_audit_details("NO_OFFER",
                                                f"INFO: No coupons returned from the coupon selector.")]
        elif user_type not in configuration['user_types']['offer_presentment_user_types']:
            audit_details += [get_audit_details("NO_OFFER",
                                                f"INFO: User type = {user_type}")]

        response = {
            'Offer': {
                'AuditDetails': audit_details
            }
        }

    print("Response", json.dumps(response))
    
    return response


def check_offer(event):
    global last_update_time
    global configuration
    global excluded_category_ids
    print("before: last_update_time: ", last_update_time)
    offer_context = event['userRequest']['offerContext']
    payload = offer_context['eventPayload']
    if reload_caching(last_update_time, caching_expiration):
        configuration = load_config(dynamodb)
        excluded_category_ids = load_exclusions(dynamodb)
        last_update_time = datetime.now()
        print("cache released: ", last_update_time)
    #configuration = load_config(dynamodb)
    payload_cart_total = parse_price_string(payload['detail']['cartTotal'])
    last_offer_minimum_spend = float(event['userRequest']['lastOffer']['Spend'])
    session_id = parse_session_id(event)
    print("Session ID", session_id)
    payload_cart_economics = parse_cart_economics(offer_context['eventPayload']['detail']['cartData'], configuration, dynamodb, excluded_category_ids)
    print("Cart Economics", json.dumps(payload_cart_economics))

    # Cross-check the session_state against the contract.
    session_state = get_session_state(session_id)
    if session_state is None:
        print("WARNING: No session_state, even though the API is operating in COMPLETE mode. "
              "Outside of testing, this indicates a problem with the feature store ingestion.")
    if session_state is not None:
        print("Session State:", json.dumps(session_state))
        try:
            feature_store_cart_total = parse_price_string(session_state['CURRENT_CART_TOTAL'])
        except KeyError:
            feature_store_cart_total = 0.0
        if round(payload_cart_total, 2) != round(feature_store_cart_total, 2):
            print(f"WARNING: The request cart total and the feature store cart "
                  f"total are not the same. ({round(payload_cart_total, 2)} != {round(feature_store_cart_total, 2)}). "
                  f"Outside of testing, this likely indicates a latency issue.")

    # Cross-check the cartData and cartTotal.
    if round(payload_cart_economics['cart_total'], 2) != round(payload_cart_total, 2):
        print(f"ERROR: The total cartData in payload does not add up to "
              f"cartTotal in payload. ({round(payload_cart_economics['cart_total'], 2)} != {round(payload_cart_total, 2)}).")

    try:
        progress_uncapped = payload_cart_economics['cart_total_eligible']
    except KeyError:
        progress_uncapped = 0.0

    away_uncapped = last_offer_minimum_spend - progress_uncapped
    if last_offer_minimum_spend < progress_uncapped:
        away = 0.0
        progress = last_offer_minimum_spend
    else:
        away = last_offer_minimum_spend - progress_uncapped
        progress = progress_uncapped

    response = {
        'Goal': round(event['userRequest']['lastOffer']['Spend'], 2),
        'Away': round(away, 2),
        'Progress': round(progress, 2),
        'MetaData': {
            'Contribution': {
                'AwayUncapped': round(away_uncapped, 2),
                'ProgressUncapped': round(progress_uncapped, 2)
            },
            'Cart': {
                'Eligible': payload_cart_economics['cart_total_eligible'],
                'Ineligible': payload_cart_economics['cart_total_ineligible']
            }
        }
    }
    print(json.dumps(response))

    return response


def assign_user_type(event):
    global last_update_time
    global configuration
    user_type = None
    if reload_caching(last_update_time, caching_expiration):
        configuration = load_config(dynamodb)
        last_update_time = datetime.now()
        print("cache released: ", last_update_time)
    cohort_number = event['userRequest']['account']['Cohort']
    test_interval = configuration['user_types']['TEST']
    control_interval = configuration['user_types']['CONTROL']
    if cohort_number in range(test_interval[0], test_interval[1] + 1):
        user_type = 'TEST'
    elif cohort_number in range(control_interval[0], control_interval[1] + 1):
        user_type = 'CONTROL'
    return user_type


def return_user_type(event):
    account_id = event['userRequest']['account']['AccountID']
    try:
        user_type = event['userRequest']['account']['UserType']
        action = "NOOP"
    except KeyError:
        user_type = assign_user_type(event)
        action = "PERSIST"
    response = {
        'AccountID': account_id,
        'Instruction': {
            'Action': action,
            'Data': {
                'userType': user_type
            }
        }
    }
    return response


def lambda_handler(event, context):
    response = None
    print("Event:", json.dumps(event))
    try:
        print("Mode:", event['mode'])
    except KeyError:
        print("`mode` attribute not found in the offer.")
    if 'mode' in event and event['mode'] == 'OFFER':
        response = create_offer(event)
    elif event['mode'] == 'COMPLETION':
        response = check_offer(event)
    elif event['mode'] == 'START_SESSION':
        response = return_user_type(event)
    # added second extra comment for testing deployment change
    if response is not None:
        return response
