import os

from botocore.exceptions import ClientError

from dynamodb_json import json_util as util

CONFIG_TABLE_NAME = os.environ['CONFIG_TABLE_NAME']


def load_exclusions(dynamodb):
    ''' Return the list of exclusions form the configdb in UPERCASE'''
    exclusions = list()
    table = dynamodb.Table(CONFIG_TABLE_NAME)

    try:
        response = table.get_item(Key={'ConfigType': 'BUSINESS_RULES_EXCLUSIONS'})
    except ClientError as e:
        print(e.response['Error']['Message'])
    else:
        if "Item" in response:
            loaded_exclusions = response['Item']['ConfigDetails']['exclusions']
    try: 
        exclusions = [x.upper() for x in loaded_exclusions]
    except AttributeError:
        exclusions = loaded_exclusions
    print('exclusions: ', exclusions)
    return exclusions


def load_config(dynamodb):
    table = dynamodb.Table(CONFIG_TABLE_NAME)

    try:
        response = table.get_item(Key={'ConfigType': 'BUSINESS_RULES'})
    except ClientError as e:
        print(e.response['Error']['Message'])
    else:
        config_file = response['Item']['ConfigDetails']

        return util.loads(config_file)
