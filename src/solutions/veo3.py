"""Original set of business rules from ES Engage / VEO 3.0"""

import json
import os
import random
from datetime import datetime
from decimal import Decimal
from math import (floor, ceil)

from boto3.dynamodb import conditions as DynamodDbConditions

from dynamodb_json import json_util
from solutions.client_specifics import is_product_eligible, get_product_margin_db
from solutions.parsing import parse_price_string

COUPON_TABLE_NAME = os.environ['COUPON_TABLE_NAME']
COUPON_TABLE_INDEX_NAME = os.environ['COUPON_TABLE_INDEX_NAME']


class VEO3:
    model_endpoint_name = 'test-runtime-feature-store-query'

    failsafe_response = {
        'PredictedAOV': 75,
        'PredictedConversionProbability': 0.1
    }

    def __init__(self, lambda_client, dynamodb, configuration, category_margins=dict()):
        self.lambda_client = lambda_client
        self.dynamodb = dynamodb
        self.configuration = configuration
        self.category_margins = category_margins
        self.audit_details = list()
        self.coupon_table = self.dynamodb.Table(COUPON_TABLE_NAME)

    def decide_on_offer_type(self, model_outputs, session_state, offer_context):
        """Decide whether to present a stretch offer, a conversion offer, or a pre-ATC offer.

        If the user is on the cart page, or the cart total is above a threshold (typically 0),
        then present a stretch offer if the cart is below the predicted order value,
        otherwise present a conversion offer.

        In other conditions, present a product offer if on a product page, otherwise,
        present a generic offer if on specific pages (e.g., home page, search page) see config.

        Outputs:
        atc_stretch
        atc_conversion
        pre_atc_product
        pre_atc_generic
        None
        """

        offer_type = None

        page_type = offer_context['context']['pageType']

        try:
            cart_total = parse_price_string(offer_context['eventPayload']['detail']['cartTotal'])
        except KeyError:
            cart_total = 0.0
            self._add_to_audit_details('CART_TOTAL',
                                       f"INFO: Cart Total {cart_total}")

        # if we are on the cart page, or the cart total is above a threshold (typically 0), then...
        if session_state is not None and (cart_total > self.configuration["general"]["minimum_cart_total_for_atc_offers"]):
            # ...then present either a stretch offer or a conversion offer

            # If the cart_total is not at the predicted value, consider a stretch offer
            if cart_total < model_outputs['PredictedAOV']:
                # TODO: Add in presented offer counts as a feature
                if int(session_state['ATC_STRETCH_OFFERS_PRESENTED']) < self.configuration['atc_stretch']['max_allowed_presentments']:
                    offer_type = 'atc_stretch'
                else:
                    offer_type = 'atc_conversion'
            else:
                offer_type = 'atc_conversion'
        else:
            # Present a pre-ATC offer
            if page_type == self.configuration['offer_triggers']['product_view']:
                # Stop pre_atc_product offer after adding an item to cart
                if cart_total == 0.0:
                    offer_type = 'pre_atc_product'  # Present a product offer
                else:
                    self._add_to_audit_details('NO_OFFER',
                                               f"INFO: Cart is not empty.")
            elif page_type in self.configuration['offer_triggers']['pre_atc_generic']:
                if cart_total == 0.0:
                    offer_type = 'pre_atc_generic'  # Present a generic offer
                else:
                    self._add_to_audit_details('NO_OFFER',
                                               f"INFO: Cart is not empty.")        
            
            else:
                self._add_to_audit_details('NO_OFFER',
                                           f"INFO: We do not recognize the page.")
                pass
        self._add_to_audit_details('OFFER_TYPE_CHOSEN',
                                   f"INFO: Offer type chosen: {offer_type}.")
        if offer_type in self.configuration["general"]["enabled_offer_types"]:
            return offer_type
        else:
            if offer_type is not None:
                self._add_to_audit_details('CHOSEN_OFFER_TYPE_NOT_ENABLED',
                                           f"INFO: Chosen offer type: {offer_type} is not enabled.")

    def get_spend_and_reward(self, offer_type, cart_economics, model_outputs, session_state, offer_context, excluded_category_ids):
        """Return the spend and the associated reward of an offer.
            - Spend is the amount of money required to fulfill the offer.
            - Reward is the monetary value of the offers that they get in return.
        """
        # TODO: Split into three or four functions, depending on the offer_type

        variant = 'default'

        if offer_type == 'pre_atc_generic':
            spend = {'total': self.configuration['pre_atc_generic']['spend']}
            reward_value = self.configuration['pre_atc_generic']['reward']

            model_metadata = {
                'is_predicted_conversion_used': False,
                'is_predicted_aov_used': False
            }
        else:  # if offer_type is pre_atc_product, atc_conversion and atc_stretch
            p = 0
            # To get the total spend, first get the eligible and ineligible portions of the spend
            spend = {'total': 0, 'eligible': 0, 'ineligible': 0}
            model_metadata = {
                'is_predicted_conversion_used': True
            }
            if offer_type == 'pre_atc_product':
                # We are on a product page, and the cart is empty (or below the configured limit).
                # Use the price of the current product calculate the spend and the reward.
                product_price = parse_price_string(offer_context['context']['productDetails']['productPrice'])
                is_item_eligible = is_product_eligible(offer_context['context']['productDetails'], self.configuration, self.dynamodb, excluded_category_ids)

                self._add_to_audit_details('PRODUCT_PAGE_ELIGIBILITY',
                                           f"INFO: Product eligibility: {is_item_eligible}")
                self._add_to_audit_details('PRODUCT_PAGE_DETAILS',
                                           f"INFO: Pre-ATC offer will be based on: " +
                                           json.dumps(offer_context['context']['productDetails']))

                if is_item_eligible:
                    spend['eligible'] = product_price * self.configuration[offer_type]['stretch_multiplier']
                    spend['ineligible'] = 0.0
                elif self.configuration[offer_type]['ineligible_products_allowed']:
                    # If the product is not eligible, this resembles a purely-ineligible cart.
                    # If ineligible products are still allowed to contribute to the spend,
                    # add one to the multiplier and use it.

                    spend['eligible'] = 0.0
                    spend['ineligible'] = product_price * (1 + self.configuration[offer_type]['ineligible_contribution_multiplier'])
                    variant = 'ineligible'

                    # Q: ineligible_contribution_multiplier is used in two different ways:
                    # When the cart is purely ineligible, we add one to the multiplier. This makes increases the spend.
                    # However, when the cart is partly eligible and partly ineligible, we use this multiplier as is, so
                    # the contribution from the spend is decreased.
                    # In essence, when "ineligible" items are allowed to be used, their weight increases when
                    # they are on their own, but their weight decreases when they are mixed with other products.
                    # What is the business or economic rationale behind this choice of use?
                else:
                    # Product is not eligible and no offer is made for ineligible products.
                    spend['eligible'] = 0.0
                    spend['ineligible'] = 0.0

                # profit_margin = get_product_margin(offer_context['context']['productDetails'],self.configuration)
                profit_margin = get_product_margin_db(self.dynamodb, self.configuration, offer_context['context']['productDetails'], self.audit_details)
                model_metadata['is_predicted_aov_used'] = False

            elif offer_type in ['atc_stretch', 'atc_conversion']:
                # Get the predicted conversion probability and the total estimated spend (AOV) without an offer
                predicted_aov = model_outputs['PredictedAOV']
                p = model_outputs['PredictedConversionProbability']

                # Is the cart made up of only ineligible items?
                # If so, we will calculate the cart total in a different way, changing the spend.
                is_cart_all_ineligible = cart_economics['cart_total_eligible'] == 0 and cart_economics['cart_total_ineligible'] > 0

                # WK: Check this logic! Putting an upper bound on model order value prediction (not trusting the model).
                # If the predicted AOV is less than the eligible cart value times a multiplier, use it.
                # Otherwise, don't use the prediction from the model,
                # multiply the cart value with a configured multiplier and use that instead.
                if offer_type == 'atc_stretch':
                    spend['eligible'] = min(cart_economics['cart_total_eligible'] * self.configuration[offer_type]['max_aov_multiplier'], predicted_aov)
                    model_metadata['is_predicted_aov_used'] = cart_economics['cart_total_eligible'] * self.configuration[offer_type]['max_aov_multiplier'] < predicted_aov
                elif offer_type == 'atc_conversion':
                    spend['eligible'] = cart_economics['cart_total_eligible'] * self.configuration[offer_type]['stretch_multiplier']
                    model_metadata['is_predicted_aov_used'] = False

                if self.configuration[offer_type]['ineligible_products_allowed']:
                    if is_cart_all_ineligible:
                        # If the cart is completely ineligible, add 1 to the ineligible_contribution_multiplier
                        # Q: What is the business rationale for this.
                        spend['ineligible'] = cart_economics['cart_total_ineligible'] * (1 + self.configuration[offer_type]['ineligible_contribution_multiplier'])
                        # Q: If the cart is completely made of ineligible products, we are not using the model at all - is this by design?
                    else:
                        spend['ineligible'] = cart_economics['cart_total_ineligible'] * self.configuration[offer_type]['ineligible_contribution_multiplier']
                else:
                    spend['ineligible'] = 0.0
                    profit_margin = cart_economics['cart_profit_eligible']

                profit_margin = (
                        (cart_economics['cart_total_eligible'] * cart_economics['cart_profit_eligible']
                         + cart_economics['cart_total_ineligible'] * self.configuration[offer_type]['blended_margin_percent'])
                        / cart_economics['cart_total']
                )
                # Q: For the ineligible parts of the cart, it seems that we are
                # using a configured margin instead of the actual profit margin.
                # What is the business rationale behind not using the actual profit margin?

                # Q: Even when ineligible products are not allowed to be used,
                # we are still using profit in their calculation.
                # Is this by design? Or should we change it?

            spend['total'] = spend['eligible'] + spend['ineligible']
            self._add_to_audit_details('SPEND', f"INFO: Spend {json.dumps(spend)}")

            reward_value = (1 - p) * spend['total'] * self.configuration[offer_type]['margin_share'] * profit_margin
            self._add_to_audit_details('REWARD_VALUE',
                                       f"""INFO: Reward Value Calculation: """
                                       f"""(1 - PredictedConversionProbability) * MarginShare * ProfitMargin = RewardValue """
                                       f"""(1 - {p}) * {spend['total']} * {self.configuration[offer_type]['margin_share']} * {profit_margin} = {reward_value}""")

        print("Spend: {} -> get {}".format(spend['total'], reward_value))
        return spend['total'], reward_value, variant

    def get_model_outputs(self, session_id):
        if self.model_endpoint_name is not None and self.model_endpoint_name:
            try:
                response = self.lambda_client.invoke(
                    FunctionName=self.model_endpoint_name,
                    InvocationType='RequestResponse',
                    Payload=json.dumps({'sessionID': session_id}),
                )
                print(f"Model Endpoint Response ({self.__class__.__name__})", response)

                payload_string = response['Payload'].read()
                payload_contents = json.loads(payload_string)
                print(f"Model Endpoint Payload ({self.__class__.__name__})", payload_contents)
                model_outputs = json.loads(payload_contents['body'])
                status_code = response['ResponseMetadata']['HTTPStatusCode']
            except self.lambda_client.exceptions.ResourceNotFoundException:
                self._add_to_audit_details('NO_MODEL_ENDPOINT',
                                           f"ERROR: Model endpoint {self.model_endpoint_name} not found.")
                model_outputs = self.failsafe_response
            except KeyError:
                self._add_to_audit_details('MODEL_ENDPOINT_INVALID_RESPONSE',
                                           f"ERROR: Model endpoint {self.model_endpoint_name} did not return a valid response.")
                model_outputs = self.failsafe_response
            except Exception as err:  # All other.
                self._add_to_audit_details('MODEL_ENDPOINT_CALL_UNCAUGHT_EXCEPTION',
                                           f"ERROR: The call to the model endpoint {self.model_endpoint_name} raised the following exception: {err}.")
                model_outputs = self.failsafe_response

            return model_outputs

    def get_maximum_minimum_spend(self, limit=5):
        """Get the highest value coupon possible
        for items with the highest spend.
        """

        condition = DynamodDbConditions.Key('ObjectType').eq('COUPON_TYPE')
        response = self.coupon_table.query(
            Limit=limit,
            ScanIndexForward=False,
            IndexName=COUPON_TABLE_INDEX_NAME,
            KeyConditionExpression=condition
        )
        coupon_types = json_util.loads(response['Items'])
        # print("Maximum MinimumSpend Coupons", json.dumps(coupon_types))
        coupon_types_with_inventory = [coupon_type['MinimumSpend'] for coupon_type in coupon_types if coupon_type['LoadedSequence'] > coupon_type['AllocatedSequence']]
        if not coupon_types_with_inventory:
            # TODO: This block is untested.
            print(f"WARNING: Coupons with the highest MinimumSpend are running out. Top {limit} coupons all have 0 inventory left. (Trying top {limit * 3}.)")
            response = self.coupon_table.query(
                Limit=limit * 3,
                ScanIndexForward=False,
                IndexName=COUPON_TABLE_INDEX_NAME,
                KeyConditionExpression=condition
            )
            coupon_types = json_util.loads(response['Items'])
            coupon_types_with_inventory = [coupon_type['MinimumSpend'] for coupon_type in coupon_types if coupon_type['LoadedSequence'] > coupon_type['AllocatedSequence']]
        maximum_minimum_spend = max(coupon_types_with_inventory)

        return maximum_minimum_spend

    def get_highest_available_coupon(self, limit=10):
        """Get the highest value coupon (spend/X) available
        """
        condition = DynamodDbConditions.Key('ObjectType').eq('COUPON_TYPE')
        response = self.coupon_table.query(
            Limit=limit,
            ScanIndexForward=False,
            IndexName=COUPON_TABLE_INDEX_NAME,
            KeyConditionExpression=condition
        )
        coupon_types = json_util.loads(response['Items'])
        # print("Lowest MinimumSpend Coupons", json.dumps(coupon_types))
        coupon_types_available = [coupon_type['MinimumSpend'] for coupon_type in coupon_types if coupon_type['LoadedSequence'] > coupon_type['AllocatedSequence']]
        if coupon_types_available:
            highest_available_coupon = max(coupon_types_available)
        else:
            highest_available_coupon = 0
        return highest_available_coupon

    def get_lowest_available_coupon(self, limit=10):
        """Get the lowest value coupon (spend/X) available
        """
        condition = DynamodDbConditions.Key('ObjectType').eq('COUPON_TYPE')
        response = self.coupon_table.query(
            Limit=limit,
            ScanIndexForward=True,
            IndexName=COUPON_TABLE_INDEX_NAME,
            KeyConditionExpression=condition
        )
        coupon_types = json_util.loads(response['Items'])
        # print("Lowest MinimumSpend Coupons", json.dumps(coupon_types))
        coupon_types_available = [coupon_type['MinimumSpend'] for coupon_type in coupon_types if coupon_type['LoadedSequence'] > coupon_type['AllocatedSequence']]
        if coupon_types_available:
            lowest_available_coupon = min(coupon_types_available)
        else: 
            lowest_available_coupon = 0        
        return lowest_available_coupon

    def get_available_coupon_types(self, spend_bounds=None):
        # Partition Key: ObjectType. Sort Key: MinimumSpend
        condition = DynamodDbConditions.Key('ObjectType').eq('COUPON_TYPE')
        if spend_bounds is not None and len(spend_bounds) == 2:
            if spend_bounds[0] > spend_bounds[1]:
                spend_bounds = reversed(spend_bounds)
            condition &= DynamodDbConditions.Key('MinimumSpend').between(Decimal(ceil(spend_bounds[0])), Decimal(floor(spend_bounds[1])))
        expression = condition.get_expression()
        if hasattr(expression['values'][0], 'get_expression'):
            expressions = list()
            for subcondition in condition.get_expression()['values']:
                expressions.append(subcondition.get_expression())
            expression = expressions
        self._add_to_audit_details('DYNAMO_DB_QUERY_CONDITIONS',
                                   f"""INFO: DynamodDbConditions: {expression}""")
        response = self.coupon_table.query(
            ProjectionExpression="CouponCode, LoadedSequence, AllocatedSequence, RewardValue, MinimumSpend",
            IndexName=COUPON_TABLE_INDEX_NAME,
            KeyConditionExpression=condition
        )
        # TODO: Add in a recursive call if the response is larger than 1MB and we only partial results.

        coupon_types = json_util.loads(response['Items'])
        # print(f"Found {len(coupon_types)} coupons.")
        coupon_types = [coupon_type for coupon_type in coupon_types if coupon_type['LoadedSequence'] > coupon_type['AllocatedSequence']]
        if not len(coupon_types):
            self._add_to_audit_details('NO_COUPONS_BETWEEN_SPEND_BOUNDS',
                                       f"""WARNING: No available coupon type found for Spend range {spend_bounds}. Please check if inventory needs to be increased.""")

        return coupon_types

    def select_coupon_types(self, spend, reward_value, number_of_coupons_requested=1):
        # TODO: The ordering may be reverse. This does not generate a problem
        # at the moment, because the number of coupons requests is 1
        # But this can turn into an issue if this lambda returns multiple coupons.
        # 2021-04-22, Sinan

        #maximum_minimum_spend = self.get_maximum_minimum_spend()
        highest_available_coupon = self.get_highest_available_coupon()
        lowest_available_coupon = self.get_lowest_available_coupon()

        if spend >= highest_available_coupon:
            self._add_to_audit_details('USING_HIGHEST_AVAILABLE_COUPON',
                                       f"""INFO: Spend {spend} is greater than {highest_available_coupon} - highest valued coupon in inventory. Using this coupon.""")
            spend_bounds = (highest_available_coupon, highest_available_coupon)
        # wk: spend must be positive; otherwise no offer should be presented
        elif 0 < spend <= lowest_available_coupon:
            self._add_to_audit_details('USING_LOWEST_AVAILABLE_COUPON',
                                       f"""INFO: Spend {spend} is lower than {lowest_available_coupon} - lowest valued coupon in inventory. Using this coupon.""")
            spend_bounds = (lowest_available_coupon, lowest_available_coupon)
        else:
            spend_bounds = (0.5 * spend, 1.0 * spend)
            # TODO: Parameterize.

        coupon_types = self.get_available_coupon_types(spend_bounds=spend_bounds)
        self._add_to_audit_details('COUPONS_BETWEEN_SPEND_BOUNDS',
                                   f"""INFO: {len(coupon_types)} coupon types found between {spend_bounds}, based on the Spend {spend}.""")
        # if len(coupon_types) < 5:
        #     print(json.dumps(coupon_types))
        # else:
        #     print(json.dumps(random.choices(coupon_types, k=5)))

        if spend > lowest_available_coupon:
            coupon_types = [coupon_type
                            for coupon_type
                            in coupon_types
                            if coupon_type['RewardValue'] <= reward_value]
            self._add_to_audit_details('COUPONS_BETWEEN_SPEND_BOUNDS_AND_REWARD',
                                       f"""INFO: {len(coupon_types)} coupon types left after filtering for reward values less than {reward_value}.""")

        # if len(coupon_types) < 5:
        #     print(json.dumps(coupon_types))
        # else:
        #     print(json.dumps(random.choices(coupon_types, k=5)))

        coupon_type_count = len(coupon_types)
        if number_of_coupons_requested > coupon_type_count:
            number_of_coupons_requested = coupon_type_count

        # Implement bubble sort to get to the closest.
        for i in range(coupon_type_count):
            for j in range(coupon_type_count - i - 1):
                coupon_type = coupon_types[j]
                next_coupon_type = coupon_types[j + 1]

                spend_difference = abs(coupon_type['MinimumSpend'] - spend) - abs(next_coupon_type['MinimumSpend'] - spend)
                reward_difference = abs(coupon_type['RewardValue'] - reward_value) - abs(next_coupon_type['RewardValue'] - reward_value)
                # Q: The entire logic seems to minimize the difference between the coupon inventory and
                # the values from the offer. I do not believe that this is the best way to do
                # this selection.

                # I want to think a bit more about
                # using Euclidean distance to select the coupon
                # The current rules would prioritize coupons
                # even if their reward values are drastically different
                # just because their spend values are close.
                # For instance, with values of 500 & 55,
                # this logic prefers 550 & 5 over 600 & 50
                # However, intuitively, 600 & 50 is closer.
                # log + Euclidean distance metric would address
                # this issue.
                # -- Sinan, 2021 April

                # TODO: Add difference in LoadedSequence and AllocatedSequence as a deciding factor.

                if coupon_type['LoadedSequence'] > coupon_type['AllocatedSequence']:
                    if spend_difference < 0:  # If the coupon type with the lower index value has the smaller spend difference...
                        # ... then move the coupon type with the lower index down, closer to the ordering we want.
                        coupon_types[j] = next_coupon_type
                        coupon_types[j + 1] = coupon_type
                    elif spend_difference == 0 and reward_difference < 0 and next_coupon_type['RewardValue'] < reward_value:
                        # If the spend difference is smaller than or equal, then check if the reward difference is
                        # smaller than or equal, and make sure that the reward on coupon is smaller.
                        # ... then move the coupon type with the lower index down, closer to the ordering we want.
                        coupon_types[j] = next_coupon_type
                        coupon_types[j + 1] = coupon_type
            if i >= number_of_coupons_requested:
                break

        # In the TSC simulator, there is logic that overrides all
        # calculations and presents the user with the highest
        # possible offer if the cart value is above $1000
        # I am choosing not to implement this logic, as it does not
        # seem to be part of a complete design but result of a
        # request from the client, TSC.
        # In particular, in stretch offers, there is already
        # logic that override AOV estimations and uses
        # the cart value instead.
        # Additional logic would likely not change the outcome.
        closest_coupon_types = coupon_types[-number_of_coupons_requested:]
        closest_coupon_types.reverse()
        print("closest_coupon_types: ", json.dumps(closest_coupon_types))

        self._add_to_audit_details('SELECTED_COUPON_TYPES',
                                   f"""INFO: Selected Coupon Types: {json.dumps(closest_coupon_types)}.""")
        return closest_coupon_types

    def _add_to_audit_details(self, code, message):
        # print(self.__class__.__name__, message)
        self.audit_details += [{
            'AuditCode': code,
            'Detail': {
                'Message': message,
                'Solution': self.configuration["general"]['client'],
                'Datetime': datetime.utcnow().strftime('%Y-%m-%dT%H:%M:%S+00:00')
            }
        }]
