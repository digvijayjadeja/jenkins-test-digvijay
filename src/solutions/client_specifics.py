import boto3
import json
import os
from datetime import datetime
from solutions.load_config import load_exclusions
from botocore.exceptions import ClientError


# dynamodb = boto3.resource('dynamodb')

# configuration = load_config(dynamodb)
# category_margins = load_margins()


# def get_product_margin(product_details, configuration):
#     profit_margin = 0
#     product_identifier_key = configuration["general"]["product_identifier_key"]
#     if 'profitMargin' in product_details:
#         profit_margin = float(product_details['profitMargin'])
#     elif product_identifier_key in product_details:
#         print(product_details[product_identifier_key])
#         try:
#             print(category_margins[product_identifier_key])
#             profit_margin = category_margins[product_identifier_key]
#         except KeyError:
#             print("WARNING: profitMargin missing from contract, and the productCategoryId "
#                   "from the contract is not found in category margins. Using default "
#                   "value for profit margin.", json.dumps(product_details))
#             profit_margin = configuration["general"]["default_profit_margin"]
#     else:
#         print("WARNING: profitMargin and productCategory not found. Using default value for the profit margin.",
#               json.dumps(product_details))
#         profit_margin = configuration["general"]["default_profit_margin"]
#     # TODO: Remove this after enforcing decimal margins in dynamodb
#     if 100 > profit_margin > 1:
#         return profit_margin / 100
#     else:
#         return profit_margin


def is_product_eligible(product_details, configuration, dynamodb, excluded_category_ids):
    is_eligible = True
    #excluded_category_ids = load_exclusions(dynamodb)
    product_identifier = product_details[configuration["general"]["product_identifier_key"]]
    try:
        product_identifier = product_identifier.upper()
    except AttributeError:
        pass
        
    if product_identifier in excluded_category_ids:
        is_eligible = False
    elif 'isEligible' in product_details:
        is_eligible = product_details['isEligible']
    elif 'eligibility' in product_details:
        is_eligible = product_details['eligibility']
    return is_eligible


def get_product_margin_db(dynamodb, configuration, product_details, audit_details):
    # print("getting margin")
    margin = configuration['general']['default_profit_margin']
    # margin = None
    tablename = os.environ['MARGIN_TABLE_NAME']
    table = dynamodb.Table(tablename)

    product_identifier_key = product_details[configuration['general']['product_identifier_key']]
    try:
        product_identifier_key = product_identifier_key.upper()
    except AttributeError:
        pass
        
    try:
        response = table.get_item(Key={'Category': product_identifier_key, 'ObjectType': 'SKU::DEFAULT'})
    except ClientError as e:
        print(e.response['Error']['Message'])
    else:
        if 'Item' in response:
            margin = float(response['Item']['ExtendedData']['Margin'])
        else:
            # print("Returning the default margin")
            audit_details += [{
                'AuditCode': 'PROFIT_MARGIN',
                'Detail': {
                    'Message': 'Margin not found. Using default value for the profit margin.',
                    'Solution': configuration["general"]['client'],
                    'Datetime': datetime.utcnow().strftime('%Y-%m-%dT%H:%M:%S+00:00')
                }
            }]

    if margin is not None and margin > 1:
        raise TypeError("The profitMargin is larger than 1")
    return margin
