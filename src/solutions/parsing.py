from solutions.client_specifics import is_product_eligible, get_product_margin_db


def parse_session_id(event):
    try:
        session_id = event['session']['SessionID']
    except KeyError:
        session_id = event['userRequest']['offerContext']['eventPayload']['sessionID']
    except KeyError:
        raise KeyError("Session ID not found in the contract.")

    return session_id


# used by create_offer and check_offer
def parse_price_string(price_string):
    try:
        return float(price_string)
    except ValueError:
        clean_price_string = price_string \
            .replace(',', '') \
            .replace('$', '')
        return float(clean_price_string)


def parse_cart_economics(cart_data, configuration, dynamodb, excluded_category_ids, audit_details=[]):
    """Get eligible & ineligible cart total, profit margin of the cart, and the profit margin broken down by eligible and ineligible."""
    product_identifier_key = configuration["general"]["product_identifier_key"]
    cart_total_eligible = 0.0
    cart_total_ineligible = 0.0
    cart_profit_eligible = 0.0
    cart_profit_ineligible = 0.0

    for line_item in cart_data:
        product_eligible = True
        product_price = parse_price_string(line_item['productPrice'])
        product_quantity = int(line_item['quantity']) if 'quantity' in line_item else 1
        try:
            product_eligible = bool(line_item['eligibility'])
        except KeyError:
            print('eligibility key is not found in cart_data.')
        # if product_identifier_key in line_item and line_item[product_identifier_key] in excluded_category_ids:
        if product_identifier_key in line_item and not is_product_eligible(line_item, configuration, dynamodb, excluded_category_ids):
            product_eligible = False

        # try:
        #     if offer_context['lastOffer']['Variant'] == 'TSC'
        #         if 'productCategory' in line_item and line_item['productCategory'] in TSC.excluded_categories:
        #             product_eligible = False
        # except KeyError:
        #     pass
        if product_eligible:
            cart_total_eligible += product_price * product_quantity
            # cart_profit_eligible += product_price * (get_product_margin(line_item, configuration))
            cart_profit_eligible += product_price * (get_product_margin_db(dynamodb, configuration, line_item, audit_details))
        else:
            cart_total_ineligible += product_price * product_quantity
            # cart_profit_ineligible += product_price * (get_product_margin(line_item, configuration))
            cart_profit_ineligible += product_price * (get_product_margin_db(dynamodb, configuration, line_item, audit_details))

    cart_total = cart_total_eligible + cart_total_ineligible

    cart_economics = {
        'cart_total': cart_total,
        'cart_total_eligible': cart_total_eligible,
        'cart_total_ineligible': cart_total_ineligible,
    }

    if cart_total:
        cart_economics['cart_eligible_percentage'] = cart_total_eligible / cart_total
        cart_economics['cart_ineligible_percentage'] = cart_total_ineligible / cart_total

    if cart_total_eligible:
        cart_economics['cart_profit_eligible'] = cart_profit_eligible / cart_total_eligible

    if cart_total_ineligible:
        cart_economics['cart_profit_ineligible'] = cart_profit_ineligible / cart_total_ineligible

    return cart_economics
