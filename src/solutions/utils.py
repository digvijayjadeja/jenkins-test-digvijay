from datetime import datetime


# May be used later after adding the feature store
def get_session_state(session_id):
    # response = feature_store_client.get_record(
    #     FeatureGroupName=SESSION_FEATURE_GROUP_NAME,
    #     RecordIdentifierValueAsString=session_id)

    # if 'Record' in response:
    #     # the session is found in the online feature store.
    #     record = response['Record']
    #     session_status = dict()
    #     for key_value_pair in record:
    #         key, val = key_value_pair.values()
    #         session_status[key] = val

    #     return session_status
    # else:
    #     utcnow = datetime.utcnow()
    #     print(f"Session ID {session_id} not found in the online feature store. {utcnow}")
    # 
    # wk: commented  all the above and added these two lines:
    utcnow = datetime.utcnow()
    # print(f"Session ID {session_id} not found in the online feature store. {utcnow}")


def get_audit_details(code, message):
    # print(message)
    return {
        'AuditCode': code,
        'Detail': {
            'Message': message,
            'Datetime': datetime.utcnow().strftime('%Y-%m-%dT%H:%M:%S+00:00')
        }
    }

def reload_caching(last_update_time, reload_duration):
    now = datetime.now()
    duration = now - last_update_time
    duration_in_min = duration.total_seconds() / 60
    if duration_in_min > reload_duration:
        return True
    else:
        return False