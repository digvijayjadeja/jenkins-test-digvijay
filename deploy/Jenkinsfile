import groovy.json.*
String buildVersion;
String environment;
String infrastructure;
String region;
String s3Bucket;
String buildName='es-engage-business-rules';
String awsCreds;
String stackName;
String userName;
Map<String,String> cfnParams = new HashMap<String,String>();

String DEFAULT_REGION = 'us-east-1';
String DEFAULT_CREDS = 'esi-labs-jenkins-ci';
String artifactBucket;

Map<String,String> infrastructureConfigMapping = new HashMap<String,String>();
infrastructureConfigMapping['labs'] =  new HashMap<String,String>();
infrastructureConfigMapping['devOps'] =  new HashMap<String,String>();
infrastructureConfigMapping['prod'] =  new HashMap<String,String>();

infrastructureConfigMapping['labs']['awsCreds'] = "esi-labs-jenkins-ci";
infrastructureConfigMapping['labs']['bucketPrefix'] =  "esi-labs-jenkins";

infrastructureConfigMapping['devOps']['awsCreds'] = "jenkins-aws-ci";
infrastructureConfigMapping['devOps']['bucketPrefix'] =  "esi-jenkins";

infrastructureConfigMapping['prod']['awsCreds'] = "jenkins-aws-prod";
infrastructureConfigMapping['prod']['bucketPrefix'] =  "esi-jenkins";

pipeline{
    agent {
        node {
            label 'ec2-fleet'
        }
    }
    parameters {
        string(name: 'version', description: 'Version to Deploy')
        string(name: 'environment', description: 'Environment to Deploy to')
        choice(name: 'infrastructure', choices: ['labs', 'devOps', 'prod'], description: 'Infrastructure to attach to')
        choice(name: 'region', choices: ["${DEFAULT_REGION}", 'ca-central-1'], description: 'Deployment Region')
        choice(name: 'buildSource', choices: ['rc', 'build'], description: 'Bucket artifacts reside in.')
    }
    stages {
        stage('Set up Deployment variables.'){
            steps{
                 wrap([$class: 'BuildUser']) {
                    script{
                        buildVersion = params.version;
                        environment = params.environment;
                        infrastructure = params.infrastructure;
                        region = params.region;
                        s3Bucket = infrastructureConfigMapping[infrastructure]["bucketPrefix"] +"-${params.buildSource}${infrastructure == 'labs' || infrastructure == 'devOps' ? "" : "-" + infrastructure}${region == 'us-east-1' ? "" : "-" + region}";
                        awsCreds = infrastructureConfigMapping[infrastructure]["awsCreds"];
                        stackName = "${buildName}-${environment}";
                        userName= BUILD_USER_ID ? BUILD_USER_ID : "jenkinsInternal";
                        s3BucketLocation = "${buildName}/${buildVersion}/${buildName}-${buildVersion}.zip"
                        // Initialize Source Code variables
                        artifactBucket = "esi-labs-jenkins-${params.buildSource}";

                        // Validate chosen settings
                        if (infrastructure == 'prod' && buildSource == 'build'){
                            throw new Exception("Only master builds can be deployed in production environments (${DEFAULT_REGION})");
                        }
                    }
                }
            }
        }

        stage('Replicate Source Code'){
            steps{
                // Download artifacts
                withAWS(credentials:"${DEFAULT_CREDS}", region:"${DEFAULT_REGION}"){
                    script {
                        s3Download(file: 'generated/artifacts/', bucket: "${artifactBucket}", path: "${buildName}/${buildVersion}/", force:true);
                    }
                }
                
                // Upload Artifacts
                withAWS(credentials:"${awsCreds}", region:"${region}"){
                    script {
                        if (s3Bucket != artifactBucket){
                            s3Upload(file: 'generated/artifacts/', bucket: "${s3Bucket}", path: "");
                        }
                    }
                }
            }
        }

        stage('Deploy Stack'){
            steps{
                withAWS(credentials:"${awsCreds}", region:"${region}"){
                    script {
                        cfnParams = ['Environment' : "${environment}", 'LambdaS3Bucket' : "${s3Bucket}", 
                                                'S3Location': "${s3BucketLocation}"];
                        
                        
                        //Create a CI stack
                        cfnUpdate(
                            stack:"${stackName}",
                            url:"https://s3.amazonaws.com/${s3Bucket}/${buildName}/${buildVersion}/cloud-formation-${buildVersion}.yaml",
                            params: cfnParams,
                            timeoutInMinutes:10, 
                            tags:[
                            "name=${stackName}",
                            "application=${buildName}",
                            "infrastructure=${infrastructure}",
                            "user=${userName}",
                            "automation:autoShutdown=false",
                            "compliance=false",    
                            "buildVersion=${buildVersion}", 
                            "environment=${environment}"], 
                            pollInterval:1000
                        )
                    }
                }
            }
        }
    }
}